<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


class ChangePasswordController extends Controller
{

       public function __construct()
    {
       $this->middleware('role:super_admin,admin,administration_manager,system_user');
    }
    public function process(Request $request)
    {

      
        $data=$request->all();

        $response =  $this->client->request($request->method(),str_replace(url(),'',$request->fullUrl()),(count($data)>0)?['form_params' => $data]:null);

        return respondWithJson($response);
    
    }

    public function changePassword(Request $request)
    {
  
        $data=$request->all();

        $response =  $this->client->request($request->method(),str_replace(url(),'',$request->fullUrl()),(count($data)>0)?['form_params' => $data]:null);

        return respondWithJson($response);

    }

    public function removePasswordResetRegister(Request $request)
    {
  
        $data=$request->all();

        $response =  $this->client->request($request->method(),str_replace(url(),'',$request->fullUrl()),(count($data)>0)?['form_params' => $data]:null);

        return respondWithJson($response);
    }

}
