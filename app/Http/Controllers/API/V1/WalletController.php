<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use GuzzleHttp\Client;


class WalletController extends Controller
{
     
    public function __construct(Client $client)
    {
        $this->client= new $client([
            'base_uri' => env('SOAP_DOMAIN'),
            'headers' => ['accept' => 'application/xml']

        ]);

        
    }



    public function pay(
        Request $request,
    )
    {

        $headers = [
            'Authorization' => $request->header('Authorization'),        
        ];

        $data=$request->all();
        $response =  $this->client->request($request->method(),str_replace(url(),'',$request->fullUrl()),['form_params' => $data,'headers' => $headers]);

        return respondWithJson($response);



     
    }

 

    public function confirm_payment(
        Request $request
    )
    {

        $headers = [
            'Authorization' => $request->header('Authorization'),        
        ];

        $data=$request->all();
        $response =  $this->client->request($request->method(),str_replace(url(),'',$request->fullUrl()),['form_params' => $data,'headers' => $headers]);

        return respondWithJson($response);


      
    }


    public function recharge(
        Request $request
    )
    {

        $headers = [
            'Authorization' => $request->header('Authorization'),        
        ];

        $data=$request->all();
        $response =  $this->client->request($request->method(),str_replace(url(),'',$request->fullUrl()),['form_params' => $data,'headers' => $headers]);

        return respondWithJson($response);


     
    }


    

    public function consult(
        Request $request
    )
    {

        $headers = [
            'Authorization' => $request->header('Authorization'),        
        ];

        $data=$request->all();
        $response =  $this->client->request($request->method(),str_replace(url(),'',$request->fullUrl()),['form_params' => $data,'headers' => $headers]);

        return respondWithJson($response);


     
    }

      
  
}
