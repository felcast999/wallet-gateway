<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use GuzzleHttp\Client;


class ServiceController extends Controller
{
     
    private $client;

    public function __construct(Client $client)
    {
        $this->client= new $client([
            'base_uri' => env('SOAP_DOMAIN'),
            'headers' => ['accept' => 'application/xml']

        ]);

        
    }
   
    public function dataTable(Request $request)
    {

        $headers = [
            'Authorization' => $request->header('Authorization'),        
        ];

        $data=[];
        $response =  $this->client->request($request->method(),str_replace(url(),'',$request->fullUrl()),['form_params' => $data,'headers' => $headers]);

        return respondWithJson($response);

    }


    public function store(
        Request $request
    )
    {

         $data=$request->all();

         $headers = [
            'Authorization' => $request->header('Authorization'),        
        ];

        $response =  $this->client->request($request->method(),str_replace(url(),'',$request->fullUrl()),['form_params' => $data,'headers' => $headers]);

        return respondWithJson($response);

     
    }

 

    public function update(
        Request $request,
        $id
    )
    {


        $data=$request->all();

        $headers = [
            'Authorization' => $request->header('Authorization'),        
        ];

        $response =  $this->client->request($request->method(),str_replace(url(),'',$request->fullUrl()),['form_params' => $data,'headers' => $headers]);

        return respondWithJson($response);



      
    }


    public function show(Request $request,
        $id
    )
    {

        $headers = [
            'Authorization' => $request->header('Authorization'),        
        ];

        $data=[];
        $response =  $this->client->request($request->method(),str_replace(url(),'',$request->fullUrl()),['form_params' => $data,'headers' => $headers]);

        return respondWithJson($response);


    }

   
     public function destroy(Request $request,
         $id
    )
    {

        $headers = [
            'Authorization' => $request->header('Authorization'),        
        ];

        $data=[];
        $response =  $this->client->request($request->method(),str_replace(url(),'',$request->fullUrl()),['form_params' => $data,'headers' => $headers]);

        return respondWithJson($response);

      
    }

    public function destroy_multiple(
        Request $request
    )
    {

        $data=$request->all();

        $headers = [
            'Authorization' => $request->header('Authorization'),        
        ];

        $response =  $this->client->request($request->method(),str_replace(url(),'',$request->fullUrl()),['form_params' => $data,'headers' => $headers]);

        return respondWithJson($response);



      
    }
  
}
