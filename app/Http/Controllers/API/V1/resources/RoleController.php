<?php

namespace App\Http\Controllers\API\V1\resources;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use GuzzleHttp\Client;

class RoleController extends Controller
{
    private $client;

    public function __construct(Client $client)
    {
        $this->client= new $client([
            'base_uri' => env('SOAP_DOMAIN'),
            'headers' => ['accept' => 'application/xml']

        ]);

        
    }

    public function get(Request $request)
    {
        $data=$request->all();

        $headers = [
            'Authorization' => $request->header('Authorization'),        
        ];

        $response =  $this->client->request($request->method(),str_replace(url(),'',$request->fullUrl()),['form_params' => $data,'headers' => $headers]);

        return respondWithJson($response);
    }



   

   
}