<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;


class ResetPasswordController extends Controller
{
    public function sendEmail(Request $request)
    {
    	
        $data=$request->all();

        $response =  $this->client->request($request->method(),str_replace(url(),'',$request->fullUrl()),(count($data)>0)?['form_params' => $data]:null);

        return respondWithJson($response);
    }

    public function validateEmail(Request $request)
    {
    
        $data=$request->all();

        $response =  $this->client->request($request->method(),str_replace(url(),'',$request->fullUrl()),(count($data)>0)?['form_params' => $data]:null);

        return respondWithJson($response);
 

    }

    public function createToken($email)
    {
       
        $data=$request->all();

        $response =  $this->client->request($request->method(),str_replace(url(),'',$request->fullUrl()),(count($data)>0)?['form_params' => $data]:null);

        return respondWithJson($response);

    }

    public function saveToken($token, $email)
    {
    
        $data=$request->all();

        $response =  $this->client->request($request->method(),str_replace(url(),'',$request->fullUrl()),(count($data)>0)?['form_params' => $data]:null);

        return respondWithJson($response);
    }

    public function sendResetPasswordEmail($email)
    {
      
        $data=$request->all();

        $response =  $this->client->request($request->method(),str_replace(url(),'',$request->fullUrl()),(count($data)>0)?['form_params' => $data]:null);

        return respondWithJson($response);
    }
}
