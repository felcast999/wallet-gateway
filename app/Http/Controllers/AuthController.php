<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use GuzzleHttp\Client;
class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */

    private $client;

    public function __construct(Client $client)
    {
        $this->client= new $client([
            'base_uri' => env('SOAP_DOMAIN'),
            'headers' => ['accept' => 'application/xml']

        ]);

        
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {

            
         $data = request(['email', 'password']);
         
         $response =  $this->client->request($request->method(),str_replace(url(),'',$request->fullUrl()),(count($data)>0)?['form_params' => $data]:null);

         return respondWithJson($response);

         
        
    }

    /**
     * registrar nuevo usuario.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request)
    {
    	$data = $request->all();

        $response =  $this->client->request($request->method(),str_replace(url(),'',$request->fullUrl()),(count($data)>0)?['form_params' => $data]:null);

        return respondWithJson($response);

    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me(Request $request)
    {   


        $headers = [
            'Authorization' => $request->header('Authorization'),        
        ];

        $data=[];
        $response =  $this->client->request($request->method(),str_replace(url(),'',$request->fullUrl()),['form_params' => $data,'headers' => $headers]);

        return respondWithJson($response);

    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(Request $request)
    {
        $headers = [
            'Authorization' => $request->header('Authorization'),        
        ];

        $data=[];
        $response =  $this->client->request($request->method(),str_replace(url(),'',$request->fullUrl()),['form_params' => $data,'headers' => $headers]);

        return respondWithJson($response);

    }


    
    
}
