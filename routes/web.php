<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/


$router->group(['prefix'=>'api'], function () use ($router) {


        
    $router->post('login', 'AuthController@login');
    $router->post('register', 'AuthController@register');

    $router->post('sendResetPasswordLink', 'ResetPasswordController@sendEmail');
    $router->post('changePassword', 'ChangePasswordController@process');


    $router->post('logout', 'AuthController@logout');
    $router->post('refresh', 'AuthController@refresh');
    $router->get('me', 'AuthController@me');


$router->group([
	'prefix' => 'v1',
	'namespace' => 'API\V1'
], function() use ($router) {



  $router->post('user-profile', 'admin\UserController@update_profile');


    #usuarios

    $router->post('/users', 'admin\UserController@store');
    $router->get('/users/{id:[\d]+}', [
        'as' => 'users.show',
        'uses' => 'admin\UserController@show'
    ]);
    $router->put('/users/{id:[\d]+}', 'admin\UserController@update');
    $router->delete('/users/{id:[\d]+}', 'admin\UserController@destroy');


    #roles
    $router->post('/roles', 'admin\RoleController@store');
    $router->get('/roles/{id:[\d]+}', [
        'as' => 'roles.show',
        'uses' => 'admin\RoleController@show'
    ]);
    $router->put('/roles/{id:[\d]+}', 'admin\RoleController@update');
    $router->delete('/roles/{id:[\d]+}', 'admin\RoleController@destroy');


    #servicios
    $router->post('/services', 'ServiceController@store');
    $router->get('/services/{id:[\d]+}', [
        'as' => 'services.show',
        'uses' => 'ServiceController@show'
    ]);
    $router->put('/services/{id:[\d]+}', 'ServiceController@update');
    $router->delete('/services/{id:[\d]+}', 'ServiceController@destroy');
   


    #billetera
   $router->post('wallets/recharge','WalletController@recharge');
   $router->get('wallets/consult','WalletController@consult');

   $router->get('wallets/confirm-payment','WalletController@confirm_payment');
   $router->post('wallets/payment','WalletController@pay');





   $router->post('users-destroy-multiple','admin\UserController@destroy_multiple');
   $router->post('roles-destroy-multiple','admin\RoleController@destroy_multiple');
   $router->post('wallets-destroy-multiple','WalletController@destroy_multiple');
   $router->post('services-destroy-multiple','ServiceController@destroy_multiple');





});
  
 

 $router->group([
	'prefix'  	=> 'v1/datatables',
	'namespace' => 'API\V1'
], function() use ($router){


//users

 $router->post('/users','admin\UserController@dataTable');


// roles

 $router->post('/roles','admin\RoleController@dataTable');

// services

$router->post('/services','ServiceController@dataTable');


});



$router->group([
	'prefix' => 'v1/resources',
	'namespace' => 'API\V1\resources'
], function() use ($router){




		#roles
		$router->get('get-roles','RoleController@get');

		
		#status

		$router->get('get-status-types','StatusController@get_type');
		$router->get('get-status/{type}','StatusController@get');




});



});

